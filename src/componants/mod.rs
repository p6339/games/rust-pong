pub mod ball;
pub mod player;

pub struct Position {
    pub x: f32,
    pub y: f32,
}

pub struct Movement {
    pub dx: f32,
    pub dy: f32,
}
