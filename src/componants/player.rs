use crate::componants::Movement;
use crate::componants::Position;

pub struct Player {
    pub pos: Position,
    pub mov: Movement,
}

impl Player {
    pub fn update(&mut self, timestep: u128) {
        if (self.pos.y <= 10.0 && self.mov.dy < 0.0)
            || (self.pos.y + 120.0 > 990.0 && self.mov.dy > 0.0)
        {
            self.mov.dy = 0.0;
        }
        self.pos.x += self.mov.dx * timestep as f32;
        self.pos.y += self.mov.dy * timestep as f32;
    }
}
