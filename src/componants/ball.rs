use crate::componants::player::Player;
use crate::componants::Movement;
use crate::componants::Position;

use crate::traits::Renderable;

pub struct Ball {
    pub pos: Position,
    pub mov: Movement,
}

impl Ball {
    pub fn update(&mut self, timestep: u128, others: Vec<&Player>) -> bool {
        // TODO: Hardcoded width is kinda ugly
        // TODO: Shouldn't bounce off the scoring wall
        if (self.pos.x + 20.0 >= 990.0 && self.mov.dx > 0.0)
            || (self.pos.x <= 10.0 && self.mov.dx < 0.0)
        {
            self.mov.dx = 0.0;
            self.mov.dy = 0.0;
            return false;
        }

        for other in others.iter() {
            if self.collide(other) {
                if (self.pos.x < 40.0 && self.mov.dx < 0.0)
                    || (self.pos.x > 940.0 && self.mov.dx > 0.0)
                {
                    if self.mov.dx > 0.0 {
                        self.mov.dx += 0.002
                    } else {
                        self.mov.dx += 0.002
                    }

                    self.mov.dx = -self.mov.dx;
                }
            }
        }

        if (self.pos.y + 20.0 >= 990.0 && self.mov.dy > 0.0)
            || (self.pos.y <= 10.0 && self.mov.dy < 0.0)
        {
            self.mov.dy = -self.mov.dy;
        }

        self.pos.x += self.mov.dx * timestep as f32;
        self.pos.y += self.mov.dy * timestep as f32;
        return true;
    }

    fn collide(&self, other: &Player) -> bool {
        let self_r = self.get_rect();
        let other_r = other.get_rect();
        return self_r.has_intersection(other_r);
    }
}
