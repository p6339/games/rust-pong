use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::WindowCanvas;

use crate::componants::ball::Ball;
use crate::componants::player::Player;

pub trait Renderable {
    fn draw(&self, canvas: &mut WindowCanvas);
    fn get_rect(&self) -> sdl2::rect::Rect;
}

impl Renderable for Player {
    fn draw(&self, canvas: &mut WindowCanvas) {
        // Set the drawing color to a darker blue.
        canvas.set_draw_color(Color::RGB(0, 153, 204));

        // Create a smaller centered Rect, filling it in the same dark blue.
        let l_bat = self.get_rect();
        let _ = match canvas.fill_rect(l_bat) {
            Ok(_) => {}
            Err(err) => panic!("failed to draw rect: {}", err),
        };
    }

    fn get_rect(&self) -> sdl2::rect::Rect {
        return Rect::new(self.pos.x as i32, self.pos.y as i32, 30, 120);
    }
}

impl Renderable for Ball {
    fn draw(&self, canvas: &mut WindowCanvas) {
        // Set the drawing color to a darker blue.
        canvas.set_draw_color(Color::RGB(0, 153, 204));
        let ball = self.get_rect();
        let _ = match canvas.fill_rect(ball) {
            Ok(_) => {}
            Err(err) => panic!("failed to draw rect: {}", err),
        };
    }

    fn get_rect(&self) -> sdl2::rect::Rect {
        return Rect::new(self.pos.x as i32, self.pos.y as i32, 20, 20);
    }
}
