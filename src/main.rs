use rand::{thread_rng, Rng};

use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::pixels::Color;
use sdl2::rect::Rect;
use sdl2::render::WindowCanvas;
use sdl2::ttf;

use std::time::{Duration, Instant};

mod componants;
use componants::ball::Ball;
use componants::player::Player;
use componants::{Movement, Position};

mod traits;
use traits::Renderable;

static PADDLE_SPEED: f32 = 0.5;
static BALL_SPEED: f32 = 1.0;
static TARGET_FRAME_RATE: u32 = 150;
static SCREEN_WIDTH: u32 = 1000;
static SCREEN_HEIGHT: u32 = 1000;

fn ai_player(paddle: &mut Player, ball: &Ball) {
    let ball_center = ball.pos.y + 10.0;
    let paddle_center = paddle.pos.y + 60.0;
    if paddle_center > ball_center {
        paddle.mov.dy = -PADDLE_SPEED
    } else if paddle_center < ball_center {
        paddle.mov.dy = PADDLE_SPEED
    } else {
        paddle.mov.dy = 0.0
    }
}

fn draw_field(canvas: &mut WindowCanvas) {
    // set the draw colour to a nice gray
    canvas.set_draw_color(Color::RGB(60, 60, 60));
    let mut pos: Position = Position { x: 490.0, y: 25.0 };
    while pos.y < 950.0 {
        let center_line: Rect = Rect::new(pos.x as i32, pos.y as i32, 20, 50);
        let _ = match canvas.fill_rect(center_line) {
            Ok(_) => {}
            Err(err) => panic!("failed to draw rect: {}", err),
        };
        pos.y += 100.0;
    }
}

fn reset_ball<R: Rng>(rng: &mut R) -> Ball {
    let ball_x_speed: f32 = rng.gen_range(BALL_SPEED * 0.25..BALL_SPEED * 0.5);
    let ball_y_speed: f32 = BALL_SPEED - ball_x_speed;

    let ball: Ball = Ball {
        pos: Position { x: 490.0, y: 490.0 },
        mov: Movement {
            dx: ball_x_speed,
            dy: ball_y_speed,
        },
    };

    return ball;
}

// handle the annoying Rect i32

fn render_string(canvas: &mut WindowCanvas, text: String, x: i32, y: i32) {
    let texture_creator = canvas.texture_creator();

    let ttf_context = ttf::init().expect("Could not Initialise font engine");
    let mut font = ttf_context
        .load_font("assets/PressStart2P-Regular.ttf", 128)
        .expect("Could not Load Font");
    font.set_style(ttf::FontStyle::NORMAL);

    let surface = font
        .render(&text)
        .blended(Color::RGBA(255, 0, 0, 255))
        .expect("Could not create font surface");

    let texture = texture_creator
        .create_texture_from_surface(&surface)
        .expect("Could not create font texture");

    // If the example text is too big for the screen, downscale it (and center irregardless)
    let target = Rect::new(x, y, 64, 64);

    canvas
        .copy(&texture, None, Some(target))
        .expect("could not load texture to canvas");
}

fn main() -> Result<(), String> {
    let mut rng = thread_rng();
    let sdl_context = sdl2::init()?;
    let video_subsystem = sdl_context.video()?;

    // Create a window
    let window = video_subsystem
        .window("Pong", SCREEN_WIDTH, SCREEN_HEIGHT)
        .position_centered()
        .build()
        .expect("could not initialize video subsystem");

    // create the canvas
    let mut canvas = window
        .into_canvas()
        .build()
        .expect("could not make a canvas");

    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();

    //  Create the player paddles
    let mut player1 = Player {
        pos: Position { x: 10.0, y: 440.0 },
        mov: Movement { dx: 0.0, dy: 0.0 },
    };
    let mut player1_score = 0;

    let mut player2 = Player {
        pos: Position { x: 960.0, y: 440.0 },
        mov: Movement { dx: 0.0, dy: 0.0 },
    };
    let mut player2_score = 0;

    let mut ball = reset_ball(&mut rng);

    // gameplay loop
    let mut event_pump = sdl_context.event_pump()?;
    let mut last_time = Instant::now();
    let mut current_time = Instant::now();
    let mut in_play = true;
    let mut scored = false;
    let frame_goal = Duration::new(0, 1_000_000_000u32 / TARGET_FRAME_RATE);
    'running: loop {
        // Handle events
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    keycode: Some(Keycode::Escape),
                    ..
                } => {
                    break 'running;
                }

                Event::KeyDown {
                    keycode: Some(Keycode::Up),
                    ..
                } => {
                    player1.mov.dy = -PADDLE_SPEED;
                }
                Event::KeyDown {
                    keycode: Some(Keycode::Down),
                    ..
                } => {
                    player1.mov.dy = PADDLE_SPEED;
                }

                Event::KeyUp {
                    keycode: Some(Keycode::Up),
                    ..
                } => {
                    player1.mov.dy = 0.0;
                }

                Event::KeyUp {
                    keycode: Some(Keycode::Down),
                    ..
                } => {
                    player1.mov.dy = 0.0;
                }

                Event::KeyDown {
                    keycode: Some(Keycode::Space),
                    ..
                } => {
                    if !in_play {
                        ball = reset_ball(&mut rng);
                        in_play = true;
                        scored = false
                    }
                }

                _ => {}
            }
        }

        let step = current_time.duration_since(last_time);

        if in_play {
            ai_player(&mut player2, &ball);

            // update objects
            player1.update(step.as_millis());
            player2.update(step.as_millis());
            in_play = ball.update(step.as_millis(), vec![&player1, &player2]);
        } else if !scored {
            if ball.pos.x <= 10.0 {
                player1_score += 1;
            } else if ball.pos.x + 20.0 >= 990.0 {
                player2_score += 1
            }
            scored = true;
        }

        // clear the canvas
        canvas.set_draw_color(Color::RGB(0, 0, 0));
        canvas.clear();
        draw_field(&mut canvas);

        // draw all the objects
        player1.draw(&mut canvas);
        player2.draw(&mut canvas);
        ball.draw(&mut canvas);

        render_string(&mut canvas, player1_score.to_string(), 218, 50);
        render_string(&mut canvas, player2_score.to_string(), 718, 50);

        canvas.present();

        // set the timers for the next loop iteration
        if step < frame_goal {
            std::thread::sleep(frame_goal - step)
        }
        last_time = current_time;
        current_time = Instant::now();
    }

    Ok(())
}
